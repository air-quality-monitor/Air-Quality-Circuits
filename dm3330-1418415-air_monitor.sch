EESchema Schematic File Version 2
LIBS:air_monitor_schematic_v2-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip_pic16mcu
LIBS:PM_lib
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Bicycle helmet air particulate monitor"
Date "2018-01-28"
Rev "2"
Comp "George Bryant 1418415"
Comment1 "Module DM3330"
Comment2 "Brunel University London"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2000 1600 1100 1100
U 5A6DC219
F0 "Sensor Unit" 60
F1 "sensor_unit.sch" 60
$EndSheet
$Sheet
S 4600 1600 1100 1100
U 5A6DC280
F0 "Base Unit" 60
F1 "base_unit.sch" 60
$EndSheet
$Sheet
S 3300 1600 1100 1100
U 5A6DC283
F0 "Logging Unit" 60
F1 "logging_unit.sch" 60
$EndSheet
$Comp
L +5V #PWR01
U 1 1 5A722997
P 6850 1650
F 0 "#PWR01" H 6850 1500 50  0001 C CNN
F 1 "+5V" H 6850 1790 50  0000 C CNN
F 2 "" H 6850 1650 50  0001 C CNN
F 3 "" H 6850 1650 50  0001 C CNN
	1    6850 1650
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5A7229AB
P 6850 1850
F 0 "#FLG02" H 6850 1925 50  0001 C CNN
F 1 "PWR_FLAG" H 6850 2000 50  0000 C CNN
F 2 "" H 6850 1850 50  0001 C CNN
F 3 "" H 6850 1850 50  0001 C CNN
	1    6850 1850
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5A7229CF
P 7250 1650
F 0 "#FLG03" H 7250 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 7250 1800 50  0000 C CNN
F 2 "" H 7250 1650 50  0001 C CNN
F 3 "" H 7250 1650 50  0001 C CNN
	1    7250 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5A7229DC
P 7250 1850
F 0 "#PWR04" H 7250 1600 50  0001 C CNN
F 1 "GND" H 7250 1700 50  0000 C CNN
F 2 "" H 7250 1850 50  0001 C CNN
F 3 "" H 7250 1850 50  0001 C CNN
	1    7250 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 1650 7250 1850
Wire Wire Line
	6850 1850 6850 1650
$EndSCHEMATC
